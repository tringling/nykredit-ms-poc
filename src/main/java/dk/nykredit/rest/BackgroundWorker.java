package dk.nykredit.rest;



import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by thomas on 15/07/16.
 */

@WebListener
public class BackgroundWorker implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        new PricelistCacheUpdateWorker().run();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}


