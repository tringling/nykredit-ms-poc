package dk.nykredit.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;
import com.rabbitmq.tools.json.JSONReader;

/**
 * Created by thomas on 15/07/16.
 */
public class PricelistCacheUpdateWorker implements Runnable {

    //private final static String QUEUE_NAME = "pricelist";

    @Override
    public void run() {

        String queueName = System.getenv().get("RABBIT_QUEUE_NAME");

        try {
            Thread.sleep(10000); // Waiting for RabbitMQ to fully start
            Connection connection = initConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(queueName, false, false, false, null);
            System.out.println(" [*] Waiting for messages on "+queueName+". To exit press CTRL+C");

            Consumer consumer = getConsumer(channel);
            channel.basicConsume(queueName, true, consumer);
        } catch (IOException e) {
            e.printStackTrace();
            //User, please take a look and correct

        } catch (TimeoutException e) {
            e.printStackTrace();
            //Lets re-connect
        } catch (InterruptedException e) {
            e.printStackTrace();
            //Lets re-connect ?
        }
    }

    private void updateCache(HashMap<String, String> eventMap){
        //TODO Lets update the cache
        System.out.println(eventMap.keySet().size());
    }

    private Consumer getConsumer(Channel channel){
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");

                JSONReader reader = new JSONReader();
                HashMap<String, String> eventMap = (HashMap<String, String>)reader.read(message);
                updateCache(eventMap);
            }
        };
    }

    private Connection initConnection() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        String rabbitHost = System.getenv().get("RABBIT_HOST");
        factory.setHost(rabbitHost);
        factory.setPort(5672);
        factory.setVirtualHost("/");
        factory.setPassword("guest");
        factory.setUsername("guest");
        Connection connection = factory.newConnection();
        return connection;
    }
}
