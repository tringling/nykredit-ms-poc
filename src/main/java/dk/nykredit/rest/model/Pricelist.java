package dk.nykredit.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Resource;

/**
 * Created by thomas on 14/07/16.
 */

@Resource
public class Pricelist {

    private String id;
    private String revision;

    @JsonProperty("sid")
    String sid;

    @JsonProperty("fromDate")
    String fromDate;

    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(String s) {
        id = s;
    }

    @JsonProperty("_rev")
    public String getRevision() {
        return revision;
    }

    @JsonProperty("_rev")
    public void setRevision(String s) {
        revision = s;
    }


    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }


    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }


}
