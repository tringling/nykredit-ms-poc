package dk.nykredit.rest;
 
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

@SuppressWarnings("restriction")
public class Server {

	// You can use this Server, when you a running outside a container like Jetty !!!
	// Remember to set environment variables for couch, on startup. Rabbit listener is not started automatically
	// We are are using a dockered Jetty !

	public static void main(String[] args) throws IOException {

		ResourceConfig config = new PackagesResourceConfig("dk.nykredit.rest");
		URI baseUri = UriBuilder.fromUri("http://localhost/").port(9998).build();
		HttpServer server = HttpServerFactory.create(baseUri, config);

		server.start();

		System.out.println("Server running");
		System.out.println("Visit: http://localhost:9998/");
		System.out.println("Hit return to stop...");
		System.in.read();
		System.out.println("Stopping server");
		server.stop(0);
		System.out.println("Server stopped");
	}
 
}

