package dk.nykredit.rest;

import dk.nykredit.rest.model.Pricelist;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.util.List;

@Path("pricelists")
public class RestAPI {

    @GET
    @Path("/{sid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("sid") String sid) {

        try {
            CouchDbConnector db = getDbConnector("test");
            Pricelist pricelist = db.get(Pricelist.class, sid); //TODO: Read from cache
            return Response.ok(pricelist).build();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
        catch(DocumentNotFoundException dnfe){
            return Response.status(404).build();
        }
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {

        try {
            CouchDbConnector db = getDbConnector("test");
            List pricelistIds = db.getAllDocIds(); //TODO: Read from cache
            return Response.ok(pricelistIds).build();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    private CouchDbConnector getDbConnector(String dbName) throws MalformedURLException {
        String couchUrl = System.getenv().get("COUCH_URL");
        HttpClient httpClient = new StdHttpClient.Builder()
                .url(couchUrl)
                .build();
        CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        CouchDbConnector db = new StdCouchDbConnector(dbName, dbInstance);
        return db;
    }

}