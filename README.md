# Nykredit MicroService Proof of Concept


## CouchDB setup`

In this setup, we run the 3 couch servers in Admin party mode -> no passwords

```
docker run -d -p 5984:5984 --name master klaemo/couchdb
docker run -d -p 5985:5984 --name slave1 klaemo/couchdb
docker run -d -p 5986:5984 --name slave2 klaemo/couchdb
```

The CouchDB administration webpage on master can now be accessed via this url `<DOCKER_URL>:5984/_utils`
Create `test` database

POST document below to Couch `<DOCKER_URL>:5984/test` , via e.g. curl or postman. Remember to `content-type: application/json` 
Repeat process with other `fromDate`s and `SID`s (SID2...SIDn) if you want more test data.


```
{
    "sid": "SID1",
    "fromDate": "2012-01-01"
}
```


## RabbitMQ setup

```
docker run -d --hostname killer-rabbit --name killer-rabbit -p 8080:15672 -p 5672:5672 rabbitmq:3-management
```

The RabbitMQ administration webpage can now be accessed via this url `<DOCKER_URL>:8080/`


## Jetty server setup

Map virtual directory with `-v` in the docker command, to the directory where the `RESTfulExample.war` file is situated, e.g. `$HOME/<PROJECT_FOLDER>/target`

-e COUCH_URL='http://192.168.99.100:5984' -e RABBIT_QUEUE='pricelist' -e RABBIT_URL='192.168.99.100'

```
docker run -d -e COUCH_URL='http://192.168.99.100:5984' -e RABBIT_QUEUE_NAME='pricelist' -e RABBIT_HOST='192.168.99.100' -v $HOME/work/java/RESTfulExample/target:/var/lib/jetty/webapps --name web-app -p 8081:8080 -p 8443:8443 jetty
```


Test with curl

```
curl http://<DOCKER_URL>:8081/RESTfulExample/pricelists
```

## Docker compose

Alternatively you can run `docker-compose up` from the rest-api project folder.

This will start up, rabbit, couch-master and the rest-api

Remember to create a `data` folder for couchdb, in the same folder